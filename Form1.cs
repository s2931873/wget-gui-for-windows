﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace FileDownloader
{
    public partial class Form1 : Form
    {
        private DataTable dt = new DataTable();
        private Task<string> download;
        private int nRowIndex;
        private queryGenerator db = new queryGenerator();
        private int tempId = 0;
        private bool shallWeExitThread = false;

        public Form1()
        {
            InitializeComponent();
            //set defaults for elements
            dt.Columns.Add("Column1", typeof(string));
            dt.Columns.Add("Column2", typeof(string));
            dt.Columns.Add("Column3", typeof(string));
            richTextBoxOutput.Visible = false;
            //load database - create tables
            db.create();
            dt = db.load();
          
                foreach (DataRow row in dt.Rows)
                {
                    string c1 = row[0].ToString();
                    string c2 = row[2].ToString();
                    start_download(c1, c2);
            }
            //load previous tasks   
            dataGridView1.DataSource = dt;
        }
       
        private void start_download(object sender, EventArgs e)
        {
            tempId++;
            string url = downloadBar.Text.ToString();
            
            string id = db.insert(url, "downloading");
            download = task(url, nRowIndex, id, false);
            create_task_icon(url, "downloading", id);
            if (shallWeExitThread)
            {
                create_task_icon(url, "Cancelled", id);
                shallWeExitThread = false;
            }
                db.delete(Convert.ToInt32(id));   
        }

        private void start_download(string url, string id)
        {
            tempId++;
            task(url, nRowIndex, id, false);
            download = task(url, nRowIndex, id, false);
            if (shallWeExitThread)
            {
                create_task_icon(url, "cancelled", id);
                shallWeExitThread = false;
            }
            dataGridView1.DataSource = dt;
            this.dataGridView1.Columns[2].Visible = false;
            
            db.delete(Convert.ToInt32(id));            
        }
        
         private Task<string> task(string url, int nRowIndex, string id, bool init)
        {
             try{
            string downloadFolder = "";
            richTextBoxOutput.Visible = true;
            richTextBoxOutput.Text = "";

                if (!init)
                {
                    FolderBrowserDialog openFolderDialog1 = new FolderBrowserDialog();

                    openFolderDialog1.ShowDialog();
                    downloadFolder = openFolderDialog1.SelectedPath.ToString();
                }
                else
                {
                    downloadFolder = Directory.GetCurrentDirectory();
                }

                Process p = new Process();

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WorkingDirectory = downloadFolder;
            p.EnableRaisingEvents = true;
            p.StartInfo.FileName = Directory.GetCurrentDirectory() + "/wget.exe";
            p.StartInfo.Arguments = @" -a wget " + url + " -c";
            p.Start();
                 //exit if needed
            if (shallWeExitThread)
            {
                p.Kill();
                create_task_icon(url, "cancelled", id);
                
                shallWeExitThread = false;
            }
            
            labelResponse.Text = "Downloading file/s";
            //wait for process to end

            labelResponse.Text = "Response";
            try
            {
                using (StreamReader sr = new StreamReader(downloadFolder + "/wget"))
                {
                    String line = sr.ReadToEnd();
                    richTextBoxOutput.Text += line;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an error in the output");
                Console.WriteLine(ex.Message);
            }

            //clean up
            p.Exited += new EventHandler(exited);
            //File.Delete(downloadFolder+"/wget");
            labelResponse.Text = "";
             }catch(Exception){}
            return null;
        }

        private void create_task_icon(string url, string status, string id)
        {
            //create task
            if (status == "downloading")
            {
                id = id + tempId;
                string[] row = new string[] { url, "Downloading", id.ToString()};
                
                dt.Rows.Add(row);

                //add
                dataGridView1.DataSource = dt;
                this.dataGridView1.Columns[2].Visible = false;

            }
            else if(status == "complete"){
                nRowIndex = dataGridView1.Rows.Count - 1;
                dataGridView1.Rows[nRowIndex].Cells[1].Value = "Complete";
            }
            else if(status == "cancelled") {
                nRowIndex = dataGridView1.Rows.Count - 1;
                dataGridView1.Rows[nRowIndex].Cells[1].Value = "Cancelled";
            }
            else if (status == "failed") { }
            else { }
        }

        private void exited(object sender, EventArgs e) {
            create_task_icon(null, "complete", null);
        }

        private void DataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.dataGridView1.Rows[e.RowIndex].Selected = true;
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                DataGridViewCell clickedCell = (sender as DataGridView).Rows[e.RowIndex].Cells[e.ColumnIndex];
                    // Here you can do whatever you want with the cell
                    this.dataGridView1.CurrentCell = clickedCell;  // Select the clicked cell, for instance
                    // Get mouse position relative to the vehicles grid
                    var relativeMousePosition = dataGridView1.PointToClient(Cursor.Position);
                    // Show the context menu
                    this.contextMenuStrip1.Show(dataGridView1, relativeMousePosition);
                    terminate_task();
            }
        }

        public void terminate_task(){
            shallWeExitThread = true;
        }

        private void Form1_Resize(object sender, System.EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                Hide();
        }

        private void notifyIcon1_DoubleClick(object sender, System.EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Maximized;
        }
    }

 }
  
