﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Collections;

namespace FileDownloader
{
    class queryGenerator
    {
        private databaseHelper db;
        public int lastInsertId;


        public void create()
        {

            try
            {
                db = new databaseHelper("tasks.d3db");
                String query = "create table if not exists tasks (id integer primary key, url varchar(100),"
                + "date_created varchar(40), date_completed varchar(40), status varchar(15), cmd varchar(200), path varchar(100))";

                db.ExecuteNonQuery(query);
            }
            catch (Exception) { throw; }
        }

        public DataTable load()
        {
            try
            {
                db = new databaseHelper();        
                DataTable tasks = new DataTable();
                String query = "select url, status, id from tasks where status = \"downloading\"";
                
                tasks = (DataTable)db.GetDataTable(query);
                // The results can be directly applied to a DataGridView control
                return tasks;

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public string insert(string url, string status)
        {
            db = new databaseHelper();
            Dictionary<String, String> data = new Dictionary<String, String>();

            data.Add("url", url);
            data.Add("date_created", DateTime.Now.ToString());
            data.Add("date_completed", "");
            data.Add("status", status);
            try
            {
                db.Insert("tasks", data);
                //get the id just inserted
                this.lastInsertId = db.lastInsertId;
                return lastInsertId.ToString();
            }
            catch (Exception)
            {
                throw;
               
               
            }
        }
        public void update(int id) {
            db = new databaseHelper();
            Dictionary<String, String> data = new Dictionary<String, String>();
            
            data.Add("status", "complete");
            data.Add("date_completed", DateTime.Now.ToString());
            try
            {
                db.Update("tasks", data, String.Format("id = {0}", id));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void delete(int id) {
            db = new databaseHelper();
            db.Delete("tasks", String.Format("id = {0}", id));
        }
    }
}

